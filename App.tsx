/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { Provider } from 'react-redux';
import createRootNavigator from './src/routes/router';
import configureStore from './src/store';

export interface Props {}

interface State {
    checkedSignIn: boolean;
    signedIn: boolean;
}

class App extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            signedIn: false,
            checkedSignIn: true
        };
    }

    render() {
        const { checkedSignIn, signedIn } = this.state;

        if (!checkedSignIn) {
            return null;
        }
        const store = configureStore();

        const Main = createRootNavigator(signedIn);
        return (
            <Provider store={store}>
                <Main />
            </Provider>
        );
    }
}

export default App;
