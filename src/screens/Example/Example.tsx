import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';

export interface Props {
    navigation: any;
}

interface State {
    enthusiasmLevel?: number;
}

class Example extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.root}>
                <Text style={styles.greeting}>Example </Text>

                <View style={styles.buttons}>
                    <View style={styles.button}>
                        <Button
                            title="-"
                            onPress={() => {}}
                            accessibilityLabel="decrement"
                            color="red"
                        />
                    </View>

                    <View style={styles.button}>
                        <Button
                            title="+"
                            onPress={() => navigation.goBack()}
                            accessibilityLabel="increment"
                            color="blue"
                        />
                    </View>
                </View>
            </View>
        );
    }
}

export default Example;

// styles
const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        alignSelf: 'center'
    },
    buttons: {
        flexDirection: 'row',
        minHeight: 70,
        alignItems: 'stretch',
        alignSelf: 'center',
        borderWidth: 5
    },
    button: {
        flex: 1,
        paddingVertical: 0
    },
    greeting: {
        color: '#999',
        fontWeight: 'bold'
    }
});
