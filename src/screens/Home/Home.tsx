import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
    HeaderBackButton,
    NavigationParams,
    NavigationScreenProps,
    NavigationState
} from 'react-navigation';
import { connect } from 'react-redux';
import { AppState } from '../../store';
import Http from '../../services/http';

export interface Props {
    navigation: NavigationScreenProps<NavigationState, NavigationParams>;
    auth: any;
}

interface State {
    enthusiasmLevel?: number;
}

class Home extends React.Component<Props, State> {
    static navigationOptions = ({ navigation }: NavigationScreenProps) => ({
        title: 'Home',
        headerLeft: (
            <HeaderBackButton
                onPress={() => {
                    navigation.navigate('Login');
                }}
            />
        )
    });

    constructor(props: Props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        Http.get('api/product/category/get').then(res => console.log(res));
    }

    render() {
        const { auth } = this.props;
        return (
            <View style={styles.root}>
                <Text style={styles.greeting}>
                    Hoşgeldiniz {auth.auth.userName}
                </Text>
            </View>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    auth: state
});

export default connect(mapStateToProps)(Home);
// styles
const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        alignSelf: 'center'
    },
    buttons: {
        flexDirection: 'row',
        minHeight: 70,
        alignItems: 'stretch',
        alignSelf: 'center',
        borderWidth: 5
    },
    button: {
        flex: 1,
        paddingVertical: 0
    },
    greeting: {
        color: '#999',
        fontWeight: 'bold'
    }
});
