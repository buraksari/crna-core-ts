import React from 'react';
import {
    Alert,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { connect } from 'react-redux';
import theme from '../../constants/theme';
import { doLogin } from '../../services/auth';
import Http, { RequestErrorResponse } from '../../services/http';
import { AppState } from '../../store';
import { login } from '../../store/auth/actions';
import { scale, moderateScale } from '../../utils/scale';
import { setToken } from '../../utils/storage';
import { RInput } from '../../components/RInput';
import { RButton } from '../../components/RButton';
import { Row } from '../../components/RRow';
import { RCheckBox } from '../../components/RCheckBox';

export interface Props {
    navigation: any;
    login: typeof login;
    auth: {};
}

interface State {
    email: string;
    password: string;
    rememberMe: boolean;
}

class Login extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            rememberMe: false
        };
    }

    logIn = async () => {
        doLogin(this.state.email, this.state.password)
            .then(async (res: any) => {
                await setToken(res.access_token);
                await Http.setTokenToHeader();

                this.props.login({
                    loggedIn: true,
                    userName: res.userName,
                    email: this.state.email,
                    password: this.state.password
                });
            })
            .then(() => this.props.navigation.navigate('Home'))
            .catch((err: RequestErrorResponse) =>
                Alert.alert('Error', `${err.message}`, [
                    { text: 'OK', onPress: () => {} }
                ])
            );
    };

    render() {
        return (
            <SafeAreaView style={styles.root}>
                <View style={styles.imageView}>
                    <Image
                        source={{
                            uri:
                                'http://icons.iconarchive.com/icons/graphicloads/100-flat/256/home-icon.png'
                        }}
                        style={styles.image}
                    />
                </View>

                <View style={styles.inputsView}>
                    <Text style={styles.text}>LOGIN TO YOUR ACCOUNT</Text>

                    <RInput
                        placeholder="Email *"
                        autoCapitalize="none"
                        onChangeText={email => this.setState({ email })}
                        value={this.state.email}
                    />
                    <RInput
                        placeholder="Password *"
                        autoCapitalize="none"
                        secureTextEntry
                        onChangeText={password => this.setState({ password })}
                        value={this.state.password}
                    />
                    <Row style={styles.row}>
                        <RCheckBox
                            title="Remember Me"
                            checked={this.state.rememberMe}
                            onPress={() =>
                                this.setState(prevState => ({
                                    rememberMe: !prevState.rememberMe
                                }))
                            }
                        />
                        <RButton
                            onPress={() => {}}
                            placeholder="Forgot Password ?"
                            style={styles.forgotButton}
                            textColor={theme.colors.blue}
                        />
                    </Row>

                    <RButton onPress={this.logIn} placeholder="Login" />
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    auth: state
});

export default connect(
    mapStateToProps,
    { login }
)(Login);

const styles = StyleSheet.create({
    root: {
        flex: 1,
        alignItems: 'center',
        margin: theme.sizes.margin
    },
    imageView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    inputsView: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'center',
        width: '100%'
    },
    image: {
        height: scale(100),
        width: scale(100)
    },
    text: {
        fontSize: moderateScale(18),
        fontWeight: '400',
        marginBottom: moderateScale(40)
    },
    row: {},
    forgotButton: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: theme.colors.transparent,
        borderColor: theme.colors.transparent
    }
});
