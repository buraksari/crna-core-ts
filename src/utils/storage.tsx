import AsyncStorage from '@react-native-community/async-storage';
import { STORAGE_KEYS } from '../constants/storageKeys';

export const token = STORAGE_KEYS.TOKEN;

// Set storage to hold user data
export const setStorage = (data: object) =>
    AsyncStorage.setItem('data', JSON.stringify(data));

export const getStorage = (key: string) => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(key)
            .then((res: any) => {
                if (res !== null) {
                    resolve(res);
                } else {
                    resolve(null);
                }
            })
            .catch((err: object) => reject(err));
    });
};

export const setToken = (value: string) => AsyncStorage.setItem(token, value);

export const getToken = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(token)
            .then((res: any) => {
                if (res !== null) {
                    resolve(res);
                } else {
                    resolve(null);
                }
            })
            .catch((err: object) => reject(err));
    });
};

export const removeTolken = () => AsyncStorage.removeItem(token);

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(token)
            .then((res: any) => {
                if (res !== null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch((err: object) => reject(err));
    });
};
