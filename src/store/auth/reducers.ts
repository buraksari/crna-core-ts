import { Reducer } from 'redux';
import { IS_LOGGED_IN, AuthState } from './types';

const initialState: AuthState = {
    loggedIn: false,
    userName: '',
    email: '',
    password: ''
};

const auth: Reducer<AuthState> = (state: AuthState = initialState, action) => {
    switch (action.type) {
        case IS_LOGGED_IN: {
            return {
                ...state,
                ...action.payload
            };
        }
        default:
            return state;
    }
};

export default auth;
