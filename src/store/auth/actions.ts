import { AuthState, IS_LOGGED_IN } from './types';

export const login = (authState: AuthState) => {
    return {
        type: IS_LOGGED_IN,
        payload: authState
    };
};
