// Describing the shape of the system's slice of state
export interface AuthState {
    loggedIn: boolean;
    userName: string;
    email: string;
    password: string;
}

// Describing the different ACTION NAMES available
export const IS_LOGGED_IN = 'IS_LOGGED_IN';

interface UpdateAuthAction {
    type: typeof IS_LOGGED_IN;
    payload: AuthState;
}

export type AuthActionTypes = UpdateAuthAction;
