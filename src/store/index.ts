import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import auth from './auth/reducers';

const reducers = combineReducers<any, any>({
    auth
});

export const rootReducer = (state: any, action: any) => {
    if (action.type === 'USER_LOGOUT') {
        state = undefined;
    }
    return reducers(state, action);
};

export type AppState = ReturnType<typeof reducers>;

export default function configureStore() {
    const middlewares = [];

    if (process.env.NODE_ENV === `development`) {
        const loggerMiddleware = createLogger({
            predicate: () => process.env.NODE_ENV === 'development'
        });

        middlewares.push(loggerMiddleware);
    }

    middlewares.push(thunkMiddleware);

    const middleWareEnhancer = applyMiddleware(...middlewares);

    const store = createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer)
    );

    return store;
}
