import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableNativeFeedback,
    TouchableOpacity,
    View
} from 'react-native';
import theme from '../constants/theme';
import { moderateScale, PlatformIOS } from '../utils/scale';

export interface ButtonProps {
    onPress: () => void;
    placeholder?: string;
    style?: {};
    textColor?: string;
}

export const RButton = ({
    onPress,
    placeholder,
    style,
    textColor = theme.colors.white
}: ButtonProps) => {
    if (PlatformIOS) {
        return (
            <TouchableOpacity
                onPress={() => onPress()}
                style={[styles.button, style]}
            >
                <Text style={[styles.text, { color: textColor }]}>
                    {placeholder}
                </Text>
            </TouchableOpacity>
        );
    } else {
        return (
            <TouchableNativeFeedback
                onPress={() => onPress()}
                background={TouchableNativeFeedback.Ripple(
                    'rgba(111,111,111,.1)',
                    false
                )}
                // style={[styles.button, style]}
            >
                <View style={[styles.button, style]}>
                    <Text style={[styles.text, { color: textColor }]}>
                        {placeholder}
                    </Text>
                </View>
            </TouchableNativeFeedback>
        );
    }
};

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height: theme.sizes.buttonHeight,
        width: '100%',
        marginVertical: moderateScale(10),
        backgroundColor: theme.colors.blue,
        borderWidth: 1,
        borderRadius: theme.sizes.radius,
        borderColor: theme.colors.ligthGray
    },
    text: {
        fontSize: moderateScale(14),
        color: theme.colors.white
    }
});
