import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import theme from '../constants/theme';
import { moderateScale } from '../utils/scale';

export interface CheckBoxProps {
    checked: boolean;
    onPress: () => void;
    title?: string;
    style?: {};
}

export const RCheckBox = ({
    checked,
    onPress,
    title,
    style
}: CheckBoxProps) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => onPress()}
            style={[styles.view, style]}
        >
            {checked ? (
                <Icon
                    name="check-box"
                    size={moderateScale(25)}
                    color={theme.colors.blue}
                />
            ) : (
                <Icon
                    name="check-box-outline-blank"
                    size={moderateScale(25)}
                    color={theme.colors.ligthBlack}
                />
            )}
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: theme.sizes.buttonHeight
    },
    text: {
        fontSize: moderateScale(14),
        margin: moderateScale(10)
    }
});
