import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import theme from '../constants/theme';
import { moderateScale } from '../utils/scale';

export interface InputProps {
    value: string;
    onChangeText: (text: string) => void;
    placeholder?: string;
    style?: {};
    autoCapitalize?: any;
    secureTextEntry?: boolean;
}
export interface InputState {
    isFocused: boolean;
}

export class RInput extends React.Component<InputProps, InputState> {
    constructor(props: InputProps) {
        super(props);

        this.state = {
            isFocused: false
        };
    }

    render() {
        return (
            <TextInput
                onChangeText={text => this.props.onChangeText(text)}
                value={this.props.value}
                style={[
                    styles.input,
                    this.props.style,
                    {
                        borderColor: this.state.isFocused
                            ? theme.colors.ligthBlack
                            : theme.colors.ligthGray
                    }
                ]}
                placeholder={this.props.placeholder}
                autoCapitalize={this.props.autoCapitalize}
                secureTextEntry={this.props.secureTextEntry}
                onFocus={() =>
                    this.setState(prevState => ({
                        isFocused: !prevState.isFocused
                    }))
                }
                onBlur={() =>
                    this.setState(prevState => ({
                        isFocused: !prevState.isFocused
                    }))
                }
            />
        );
    }
}

const styles = StyleSheet.create({
    input: {
        height: theme.sizes.inputHeight,
        width: '100%',
        padding: moderateScale(10),
        margin: moderateScale(10),
        fontSize: moderateScale(14),
        borderWidth: 1,
        borderRadius: theme.sizes.radius,
        borderColor: theme.colors.ligthGray
    }
});
