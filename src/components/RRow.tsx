import React, { ReactNode } from 'react';
import { StyleSheet, View } from 'react-native';

export interface RowProps {
    style?: {};
    children: ReactNode;
}

export const Row = ({ style, children }: RowProps) => {
    return <View style={[styles.row, style]}>{children}</View>;
};

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    }
});
