import {
    createAppContainer,
    createStackNavigator,
    createSwitchNavigator
} from 'react-navigation';
import Example from '../screens/Example/Example';
import Home from '../screens/Home/Home';
import Login from '../screens/Login/Login';

const screensStack = createStackNavigator(
    {
        Home: { screen: Home },
        Example: { screen: Example }
    },
    {
        headerMode: 'screen'
    }
);

const SignedOut = createStackNavigator(
    {
        Login: {
            screen: Login
        }
    },
    {
        headerMode: 'none'
    }
);

const createRootNavigator = (signedIn: boolean) => {
    return createAppContainer(
        createSwitchNavigator(
            {
                SignedIn: {
                    screen: screensStack
                },
                SignedOut: {
                    screen: SignedOut
                }
            },
            {
                initialRouteName: signedIn ? 'SignedIn' : 'SignedOut'
            }
        )
    );
};

export default createRootNavigator;
