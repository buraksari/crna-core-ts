import { moderateScale } from '../utils/scale';

const colors = {
    accent: '#F3534A',
    primary: '#0AC4BA',
    secondary: '#2BDA8E',
    tertiary: '#FFE358',
    ligthBlack: '#434754',
    black: '#323643',
    white: '#FFFFFF',
    blue: '#039be5',
    gray: '#9DA3B4',
    ligthGray: '#C5CCD6',
    transparent: '#FFFFFF00'
};

const sizes = {
    // global sizes
    base: 16,
    font: 14,
    radius: moderateScale(4),
    padding: moderateScale(25),
    margin: moderateScale(30),
    // font sizes
    h1: 26,
    h2: 20,
    h3: 18,
    title: 18,
    header: 16,
    body: 14,
    caption: 12,
    // component sizes
    inputHeight: moderateScale(50),
    buttonHeight: moderateScale(40)
};

const fonts = {
    h1: {
        fontSize: sizes.h1
    },
    h2: {
        fontSize: sizes.h2
    },
    h3: {
        fontSize: sizes.h3
    },
    header: {
        fontSize: sizes.header
    },
    title: {
        fontSize: sizes.title
    },
    body: {
        fontSize: sizes.body
    },
    caption: {
        fontSize: sizes.caption
    }
};

export default { colors, sizes, fonts };
