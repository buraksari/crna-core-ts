import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { getToken } from '../utils/storage';

export default class Http {
    static instance = axios.create({
        baseURL: 'http://interviewapp.oymakyazilim.com/',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    static async setTokenToHeader() {
        const token = await getToken();

        if (token) {
            this.instance.defaults.headers.common[
                'Authorization'
            ] = `Bearer ${token}`;
        } else {
            delete this.instance.defaults.headers.common['Authorization'];
        }
    }

    static post(
        url: string,
        data?: any,
        config?: AxiosRequestConfig
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.instance
                .post(url, data, config)
                .then(res => {
                    resolve(res.data);
                })
                .catch((error: AxiosError) => {
                    reject(this.getErrorMessage(error));
                });
        });
    }

    static get(url: string, config?: AxiosRequestConfig): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.instance
                .get(url, config)
                .then(res => {
                    resolve(res.data);
                })
                .catch((error: AxiosError) => {
                    reject(this.getErrorMessage(error));
                });
        });
    }

    static update(
        url: string,
        data?: any,
        config?: AxiosRequestConfig
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.instance
                .put(url, data, config)
                .then(res => {
                    resolve(res.data);
                })
                .catch((error: AxiosError) => {
                    reject(this.getErrorMessage(error));
                });
        });
    }

    static getErrorMessage(error: AxiosError): RequestErrorResponse {
        console.log(error.response);
        if (error.response) {
            return {
                message:
                    (error.response.data as ErrorMessage).Message ||
                    error.response.data.error_description,
                statusCode: error.response.status
            };
        } else if (error.request) {
            return {
                message: 'Bağlantı hatası.'
            };
        } else {
            return { message: error.message };
        }
    }
}

interface ErrorMessage {
    Message: string;
}

export interface RequestErrorResponse {
    message: string;
    statusCode?: number;
}
