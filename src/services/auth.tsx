import Http from '../services/http';

// export const login = (username: string, password: string): Promise<Login> => {
//     return new Promise((resolve, reject) => {
//         return Api.instance
//             .post(
//                 'token',
//                 `username=${username}&password=${password}&grant_type=password`
//             )
//             .then(data => {
//                 resolve(data.data as Login);
//             })
//             .catch((error: AxiosError) => {
//                 reject(Api.getErrorMessage(error));
//             });
//     });
// };

export const doLogin = (username: string, password: string): Promise<Login> => {
    return Http.post(
        'token',
        `username=${username}&password=${password}&grant_type=password`
    ).then(res => {
        return res;
    });
};

interface Login {
    access_token: string;
    token_type: string;
    expires_in: number;
    userName: string;
    '.issued': string;
    '.expires': string;
}
